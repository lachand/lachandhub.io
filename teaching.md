---
layout: default
title: Teaching
permalink: /teaching
---
# 2018-2019

## [Knowledge Management course](https://valentin.lachand.net/teaching/2019/km-2019)
Intervention at the INSA de Lyon in a course about data exploitation

## [LaTeX course](https://valentin.lachand.net/teaching/2019/latex-2019)
Intervention at the INSA de Lyon in a course about LaTeX and Overleaf

## [XML et Web](https://valentin.lachand.net/teaching/2019/cci-xml-web-2019)
Course for the CCI Master of Université Lyon 1

## [Conception des systèmes d'information et génie logiciel](https://valentin.lachand.net/teaching/2018/cci-uml-2018)
Course co-animated with [Alix Ducros](https://krlx.fr)

# 2017-2018

## [XML et Web](http://tabard.fr/cours/2018/xmlweb)
Course under the direction of Aurélien Tabard

## [M1if01 Gestion de projet et génie logiciel](http://tabard.fr/cours/2017/mif01/)
Course under the direction of Aurélien Tabard

# 2016-2017

## [XML et Web](http://tabard.fr/cours/2018/xmlweb)
Course under the direction of Aurélien Tabard
